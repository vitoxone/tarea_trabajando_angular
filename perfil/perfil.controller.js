﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('PerfilController', PerfilController);

    PerfilController.$inject = ['$location', '$rootScope', 'AuthService'];
    function PerfilController($location, $rootScope, AuthService) {
        var vm = this;

        vm.user = null;
        vm.allUsers = [];

        iniciarControlador();

        function iniciarControlador() {

            var loggedIn = $rootScope.globals.currentUser;
            if (!loggedIn) {
                $location.path('/login');
            }

            vm.user = $rootScope.globals.currentUser.usuario;
            vm.foto = $rootScope.globals.currentUser.foto;
            vm.edad = $rootScope.globals.currentUser.edad;
            vm.direccion = $rootScope.globals.currentUser.direccion;

            CargarGaleria();

        }

        function CargarGaleria() {
            AuthService.CargarGaleria(function (data) {
                if(data.data != null){
                    for(var i = 0; i<data.data.length; i++){
                        $('.bxslider').append('<li><img src="'+data.data[i]+'"></li>');
                    }   
                }
            $('.bxslider').bxSlider({
              mode: 'horizontal',
              useCSS: true,
              infiniteLoop: true,
              hideControlOnEnd: true,
              ticker: false,
              slideWidth: 600,
              speed: 2000,
              auto: true
            });    
            });
        }
    }

})();