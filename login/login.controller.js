﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$location', 'AuthService', 'MensajesService'];
   
    function LoginController($location, AuthService, MensajesService) {
        var vm = this;

        vm.login = login;

        //Se borran los datos del usuario
        (function inicializarControlador() {
            AuthService.BorrarSession();
        })();

        function login() {
            AuthService.Login(vm.username, vm.password, function (response) {
                if (response.success) {
                    AuthService.GuardarSession(response.message, response.data.name, response.data.photo, response.data.age, response.data.address);
                    $location.path('/');
                } else {
                    MensajesService.Error(response.message);
                }
            });
        };
    }

})();
