﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('AuthService', Autentificar);

    Autentificar.$inject = ['$http', '$cookieStore', '$rootScope'];

    function Autentificar($http, $cookieStore, $rootScope) {
        var service = {};

        service.Login = Login;
        service.GuardarSession = GuardarSession;
        service.BorrarSession =  BorrarSession;
        service.CargarGaleria =  CargarGaleria;

        return service;


        function Login(usuario, password, callback) {

            //Simulacion de datos debido a la inestabilidad del servicio
            var datos = {
                        success : true,
                        message : 'Bienvenido',
                        data: { 
                                "name" : 'Juan',
                                "photo"  : 'http://www.coordinadora.com/wp-content/uploads/sidebar_usuario-corporativo.png',
                                "age"       : 24,
                                "address" : 'Mi casa'
                            }
                        };
  
            callback(datos);

            //consulta real al servicio
          /*  $.post('http://notificador.demotbj.com/logintest',{user:usuario, password: password}, function(data){

               if(data.success){
                callback(data);
               }

            });*/

        }
        function CargarGaleria(callback){
         $.getJSON( "http://notificador.demotbj.com/catalogtest", function( data ) {
                if(data.data != null){
                    callback(data);
                }
            })
            .fail(function(jqXHR, textStatus, errorThrown) { alert('Error al obtener el listado de imagenes, recarge la pagina!!'); });
        }   

        function GuardarSession(mensaje, usuario, foto, edad, direccion) {
            var datos_usuario = mensaje + ':' + usuario+ ':' + foto + ':' + edad + ':' + direccion;

            $rootScope.globals = {
                currentUser: {
                    usuario: usuario,
                    foto: foto,
                    edad: edad,
                    direccion:direccion
                }
            };

            $cookieStore.put('globals', $rootScope.globals);
        }

        function BorrarSession() {
            $rootScope.globals = {};
            $cookieStore.remove('globals');
            $http.defaults.headers.common.Authorization = 'Basic';
        }
    }

})();